run_cljscript() {
  script="$1"
  shift
  run "${BATS_TEST_DIRNAME}/../libexec/cljscript" "${BATS_TEST_DIRNAME}/e2e/${script}" "$@"
}

@test "sample.clj" {
  run_cljscript "sample.clj"

  [ $status -eq 0 ]
  [ "$output" == "Hello World" ]
}

@test "with-main.clj: no args" {
  run_cljscript 'with-main.clj'

  [ $status -eq 0 ]
  [ "$output" == ":args nil" ]
}

@test "with-main.clj: with args" {
  run_cljscript 'with-main.clj' 1 2 'and 3'
echo $output
  [ $status -eq 0 ]
  [ "$output" == ":args (\"1\" \"2\" \"and 3\")" ]
}

@test "with-deps.clj" {
  run_cljscript 'with-deps.clj'
  

  [ $status -eq 0 ]

  echo $output
  [[ "$output" =~ "1986-10-14T00:00:00.000Z" ]]
}
